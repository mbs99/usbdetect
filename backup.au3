$wbemFlagReturnImmediately = 0x10
$wbemFlagForwardOnly = 0x20
$colItems = ""
$strComputer = "localhost"
$MyDrive = "STORE N GO"

$objWMIService = ObjGet("winmgmts:\\" & $strComputer & "\root\CIMV2")
$m_MediaConnectWatcher = $objWMIService.ExecNotificationQuery("SELECT * FROM __InstanceCreationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_DiskDrive'")

While 1
    $mbo = $m_MediaConnectWatcher.NextEvent
    $obj = $mbo.TargetInstance
    If $obj.InterfaceType == "USB" Then
        $New = GetDriveLetterFromDisk($obj.Name)
        $Label = DriveGetLabel($New)
        If $Label == $MyDrive Then
            MsgBox(4096, "Info", "My drive has been plugged in, backup my files!")
        EndIf
    EndIf
WEnd

Func GetDriveLetterFromDisk($name)
    $ans = ""
    $name = StringReplace($name,"\","\\")
    $oq_part = $objWMIService.ExecQuery("ASSOCIATORS OF {Win32_DiskDrive.DeviceID=""" & $name & """} WHERE AssocClass = Win32_DiskDriveToDiskPartition", "WQL", $wbemFlagReturnImmediately + $wbemFlagForwardOnly)
    If IsObj($oq_part) Then
        For $obj_part In $oq_part
            $oq_disk = $objWMIService.ExecQuery("ASSOCIATORS OF {Win32_DiskPartition.DeviceID=""" & $obj_part.DeviceID & """} WHERE AssocClass = Win32_LogicalDiskToPartition", "WQL", $wbemFlagReturnImmediately + $wbemFlagForwardOnly)
            If IsObj($oq_disk) Then
                For $obj_disk in $oq_disk
                    $ans = $ans & $obj_disk.Name
                Next
            EndIf
        Next
    EndIf

    Return $ans
EndFunc